<?php
/**
 * 'Linkedin.php' Model class, Here all validation for 'linkedin' value is written
 *
 * @category    RedboxDigital
 * @package     RedboxDigital_Linkedin
 * @author      Shashidhar
 */
class RedboxDigital_Linkedin_Model_Linkedin extends Mage_Customer_Model_Customer
{

    /**
     * Max Lenght of the Linkedin Profile URL
     */
    private $_maxLinkedinUrlLen = 250;

    /**
     * validation on 'linkedin' attribute after parent is validated.
     * @return boolean|array 
     */
    public function validate()
    {
        $errors = parent::validate();
        $errors = ($errors === true) ? array() : $errors;
        
        $dataError = false;
        $linkedinUserProfile = trim($this->getLinkedin());
        if (Mage::helper('linkedin')->isLinkedinVisible()) {
            // Check for empty string
            if (! Zend_Validate::is($linkedinUserProfile, 'NotEmpty')) {
                $errors[] = Mage::helper('customer')->__('Linkedin Profile cannot be empty');
                $dataError = true;
            }
            
            // Check for Max and Min Characters
            if (! $dataError) {
                $validator = new Zend_Validate_StringLength();
                $validator->setMax($this->_maxLinkedinUrlLen);
                if (! $validator->isValid($linkedinUserProfile)) {
                    $errors[] = Mage::helper('customer')->__('URL is too long, max of ' . $this->_maxLinkedinUrlLen . ' characters are allowed');
                }
            }
            
            // Check for valid Linked URL
            if (! $dataError) {
                $linkedinUrlObj = Zend_Uri_Http::fromString($linkedinUserProfile);
                if (! $linkedinUrlObj->valid()) {
                    $errors[] = Mage::helper('customer')->__('Invalid URI format');
                } elseif ('.linkedin.com' != substr($linkedinUrlObj->getHost(), - 13)) {
                    $errors[] = Mage::helper('customer')->__('Please provide a valid Linkedin Profile Url.');
                }
            }
        }
        
        if (empty($errors)) {
            return true;
        }
        
        return $errors;
    }
}