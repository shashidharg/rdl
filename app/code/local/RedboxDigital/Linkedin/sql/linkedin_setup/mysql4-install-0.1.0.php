<?php
/**
 * Add 'linkedin' attribute to customer entity and 'linked_show' flag for admin control
 * 
 * @category    RedboxDigital
 * @package     RedboxDigital_Linkedin
 * @author      Shashidhar
 */
$installer = $this;
$installer->startSetup();
$setup = Mage::getModel('customer/entity_setup', 'core_setup');
$setup->addAttribute('customer', 'linkedin', array(
        'type' => 'varchar',
        'input' => 'text',
        'label' => 'Linkedin URL',
        'visible' => true,
        'required' => true,
        'default' => '',
        'unique' => true,
        'note' => 'Customers Linkedin URL',
        'position' => 100,
        'frontend_class' => 'validate-url validate-length maximum-length-250'
    )
);

Mage::getSingleton('eav/config')->getAttribute('customer', 'linkedin')
    ->setData('used_in_forms', array(
            'adminhtml_customer',
            'customer_account_create',
            'customer_account_edit'
        )
    )->save();

$core = new Mage_Core_Model_Config();
$core->saveConfig('customer/create_account/linkedin_show', '1');

$installer->endSetup();