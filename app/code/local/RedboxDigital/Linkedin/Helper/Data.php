<?php
/**
 * 'Data.php' Helper Class
 *
 * @category    RedboxDigital
 * @package     RedboxDigital_Linkedin
 * @author      Shashidhar
 */
class RedboxDigital_Linkedin_Helper_Data extends Mage_Core_Helper_Abstract
{

    const XML_PATH_LINKEDIN_SHOW = 'customer/create_account/linkedin_show';

    private $_attributes;

    /**
     * To check if 'linkedin' attribute is enabled or not
     * @return boolean
     */
    public function isLinkedinVisible()
    {
        return (bool) Mage::getStoreConfig(self::XML_PATH_LINKEDIN_SHOW);
    }

    /**
     * Get 'frontend_class' values for 'linkedin' attribute
     * @return string
     */
    public function getValidationClass()
    {
        $attribute = isset($this->_attributes) ? $this->_attributes : Mage::getSingleton('eav/config')->getAttribute('customer', 'linkedin');
        $class = $attribute ? $attribute->getFrontend()->getClass() : '';
        return $class;
    }
}